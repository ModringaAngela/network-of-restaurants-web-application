package com.restaurantsnetwork.controller;

import com.restaurantsnetwork.*;
import com.restaurantsnetwork.dao.UserRepository;
import com.restaurantsnetwork.entity.ResponseMessage;
import com.restaurantsnetwork.entity.User;
import com.restaurantsnetwork.entity.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping(path = "api/")
public class UserController {

    @Autowired
    public UserRepository userRepository;

    /*
        Post request method to save User DTO
     */
    @PostMapping("/users/save")
    public ResponseEntity<ResponseMessage> saveUser(@RequestBody UserDto userDTO) {

        // create the User object
        User newUser = new User(
                userDTO.userFirstName,userDTO.userLastName,userDTO.userDob,userDTO.typeOfUser,userDTO.emailAddress, userDTO.password, userDTO.username);

        User _User = userRepository.save(newUser);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage("registered"));
    }
}
