package com.restaurantsnetwork.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class UserDto {
    public Integer userId;

    public String userFirstName;

    public String userLastName;

    public String userDob;

    public String typeOfUser;

    public String emailAddress;

    public String password;

    public String username;
}
