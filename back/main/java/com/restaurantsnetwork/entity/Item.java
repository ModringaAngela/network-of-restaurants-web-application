package com.restaurantsnetwork.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "item")
@Data
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "restaurant_id")
    private Integer restaurantId;

    @Column(name = "name")
    private String name;

    @Column(name = "category")
    private String category;

    @Column(name = "price")
    private String price;

    @Column(name = "weight")
    private Integer weight;

    @Column(name = "observations")
    private Float observations;

    @Column(name = "photo")
    private String photo;

}
