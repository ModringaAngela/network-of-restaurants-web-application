package com.restaurantsnetwork.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "restaurant")
@Data
public class Restaurant {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String restaurantName;

    @Column(name = "address")
    private String restaurantAddress;

    @Column(name = "program")
    private String restaurantProgram;

    @Column(name = "number_of_tables")
    private Integer numberOfTables;

    @Column(name = "reviewRate")
    private Float reviewRate;

    @Column(name = "photo")
    private String restaurantPhoto;

}
