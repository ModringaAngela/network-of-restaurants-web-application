package com.restaurantsnetwork.entity;

import lombok.Data;
import javax.persistence.*;

@Entity
@Table(name = "user")
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer userId;

    @Column(name = "first_name")
    private String userFirstName;

    @Column(name = "last_name")
    private String userLastName;

    @Column(name = "dob")
    private String userDob;

    @Column(name = "type_of_user")
    private String typeOfUser;

    @Column(name = "email_address")
    private String emailAddress;

    @Column(name = "password")
    private String password;

    @Column(name = "username")
    private String username;

    public User(){

    }

    public User(String userFirstName, String userLastName, String userDob, String typeOfUser, String emailAddress, String password, String username) {
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.userDob = userDob;
        this.typeOfUser = typeOfUser;
        this.emailAddress = emailAddress;
        this.password = password;
        this.username = username;
    }
}
