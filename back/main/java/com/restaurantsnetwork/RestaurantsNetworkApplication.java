package com.restaurantsnetwork;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestaurantsNetworkApplication {

	public static void main(String[] args) {

		SpringApplication.run(RestaurantsNetworkApplication.class, args);
	}

}
