package com.restaurantsnetwork.dao;

import com.restaurantsnetwork.entity.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
public interface RestaurantRepository extends JpaRepository<Restaurant, Integer> {

}
