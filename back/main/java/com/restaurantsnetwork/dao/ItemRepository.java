package com.restaurantsnetwork.dao;
import com.restaurantsnetwork.entity.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
public interface ItemRepository extends JpaRepository<Item, Integer> {
}
