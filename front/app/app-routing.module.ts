import {NgModule} from '@angular/core';
import{Routes, RouterModule} from '@angular/router';

import { HomeComponent } from './components/home/home.component';

//login
import { ClientLoginComponent } from './components/log-in/client-login/client-login.component';
import { GeneralAdminLoginComponent } from './components/log-in/general-admin-login/general-admin-login.component';
import { AdministratorLoginComponent } from './components/log-in/administrator-login/administrator-login.component';
import { ShipperLoginComponent } from './components/log-in/shipper-login/shipper-login.component';

//users page
import { GeneralAdminComponent } from './components/users/general-admin/general-admin.component';
import { AdministratorComponent } from './components/users/administrator/administrator.component';
import { ShipperComponent } from './components/users/shipper/shipper.component';
import { ClientComponent } from './components/users/client/client.component';

import { RestaurantListComponent } from './components/restaurant-list/restaurant-list.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { ClientAccountComponent } from './components/views/client-account/client-account.component';
import { ClientReviewsComponent } from './components/views/client-reviews/client-reviews.component';
import { ClientOrdersComponent } from './components/views/client-orders/client-orders.component';
import { RestaurantComponent } from './components/restaurant/restaurant.component';
import { ItemListComponent } from './components/item-list/item-list.component';
import { CartDetailsComponent } from './components/cart-details/cart-details.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { RegisterShipperComponent } from './components/register-shipper/register-shipper.component';
import { ReviewComponent } from './components/review/review.component';


const routes : Routes = [
    {path: 'home' , component:HomeComponent},
    {path:'login-client', component:ClientLoginComponent},
    {path:'login-administrator', component:AdministratorLoginComponent},
    {path:'login-shipper', component:ShipperLoginComponent},
    {path: 'login-general-admin', component:GeneralAdminLoginComponent},
    {path:'client', component:ClientComponent},
    {path:'shipper', component:ShipperComponent},
    {path:'general', component:GeneralAdminComponent},
    {path:'administrator',component:AdministratorComponent},
    {path:'users', component:UserListComponent},
    {path: 'restaurants', component: RestaurantListComponent},
    {path:'client-account', component: ClientAccountComponent},
    {path:'client-reviews', component:ClientReviewsComponent},
    {path:'client-orders', component:ClientOrdersComponent},
    {path:'restaurant/:id', component:RestaurantComponent},
    {path:'item/:id', component:ItemListComponent},
    {path:'cart-details', component:CartDetailsComponent},
    {path:'checkout', component:CheckoutComponent},
    {path:'register-shipper', component: RegisterShipperComponent},
    {path: 'review/:id', component:ReviewComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports:[RouterModule]
})

export class AppRoutingModule{
}

export const routingComponents = [HomeComponent,
    GeneralAdminComponent, AdministratorComponent, ClientComponent, ShipperComponent,
    GeneralAdminLoginComponent, AdministratorLoginComponent, ClientLoginComponent, ShipperLoginComponent,
    RestaurantListComponent, UserListComponent,
    ClientAccountComponent,ClientReviewsComponent, ClientOrdersComponent,
    RestaurantComponent, ItemListComponent,CartDetailsComponent, CheckoutComponent,
    RegisterShipperComponent, ReviewComponent]