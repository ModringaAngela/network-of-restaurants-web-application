import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CartItem } from 'src/app/common/entities/cart-item';
import { Item } from 'src/app/common/entities/item';
import { CartService } from 'src/app/services/cart.service';
import { ItemService } from 'src/app/services/item.service';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {

  items: Item[];
  restaurantId: number = 2;

  constructor(private itemService: ItemService,
    private cartService: CartService, private route: ActivatedRoute) { }

  ngOnInit(){
      this.listItems(this.restaurantId);
  }

  listItems(theRestaurantId: number){
    this.itemService.getItemList(theRestaurantId).subscribe(
      data => {
        this.items = data;
      }
    )
  }

  addToCart(theItem: Item){
    const theCartItem = new CartItem(theItem);
    this.cartService.addToCart(theCartItem);
  }
}
