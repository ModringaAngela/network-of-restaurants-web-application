import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Restaurant } from 'src/app/common/entities/restaurant';
import { RestaurantService } from 'src/app/services/restaurant.service';

@Component({
  selector: 'app-restaurant-list',
  templateUrl: './restaurant-list.component.html',
  styleUrls: ['./restaurant-list.component.css']
})
export class RestaurantListComponent implements OnInit {

  restaurants: Restaurant[];

  constructor(private restaurantService: RestaurantService, private route: ActivatedRoute) { }

  ngOnInit(){
    this.route.paramMap.subscribe(()=> {
      this.listRestaurants();
    })
  }

  listRestaurants(){
    this.restaurantService.getRestaurantList().subscribe(
      data => {
        this.restaurants = data;
      }
    )
  }
}
