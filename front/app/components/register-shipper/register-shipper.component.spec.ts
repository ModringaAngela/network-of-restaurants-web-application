import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterShipperComponent } from './register-shipper.component';

describe('RegisterShipperComponent', () => {
  let component: RegisterShipperComponent;
  let fixture: ComponentFixture<RegisterShipperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterShipperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterShipperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
