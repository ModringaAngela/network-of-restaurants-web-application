import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/common/entities/user';
import { LoginService} from 'src/app/services/login.service';
import { RegisterService } from 'src/app/services/register.service';

@Component({
  selector: 'app-register-shipper',
  templateUrl: './register-shipper.component.html',
  styleUrls: ['./register-shipper.component.css']
})
export class RegisterShipperComponent implements OnInit {

  titleMessage: string = '';
  signupMessage : string = '';
  constructor(private router: Router, public registerService: RegisterService) { }

  ngOnInit(): void {
  }

  onSubmit(registerForm: NgForm ) {

      let newShipper = new User();
      newShipper.username = registerForm.value['username'];
      newShipper.password = registerForm.value['password'];
      newShipper.emailAddress = registerForm.value['emailAddress'];
      newShipper.userFirstName = registerForm.value['userFirstName'];
      newShipper.userLastName = registerForm.value['userLastName'];
      newShipper.typeOfUser = "shipper";
      console.log(newShipper);

      // post client to backend to save in data base
      this.registerService.createUser(newShipper).subscribe(
        event => {  // handle success
          this.titleMessage = 'Success';
          this.signupMessage = 'Successful registration. You can login now';
          this.router.navigate(['/general']);
      });
  }
}
