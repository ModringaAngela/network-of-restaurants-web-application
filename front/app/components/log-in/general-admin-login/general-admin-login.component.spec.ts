import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralAdminLoginComponent } from './general-admin-login.component';

describe('GeneralAdminLoginComponent', () => {
  let component: GeneralAdminLoginComponent;
  let fixture: ComponentFixture<GeneralAdminLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GeneralAdminLoginComponent]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralAdminLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
