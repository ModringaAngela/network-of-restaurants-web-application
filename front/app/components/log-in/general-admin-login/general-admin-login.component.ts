import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-general-admin-login',
  templateUrl: './general-admin-login.component.html',
  styleUrls: ['./general-admin-login.component.css']
})
export class GeneralAdminLoginComponent implements OnInit {

  constructor(private router: Router, public loginService: LoginService) { }

  ngOnInit(): void {
  }

  onSubmit(loginForm: NgForm){
    if(loginForm.value.accessCode == "administrator"){
      this.router.navigate(['/general']);
    }
  }
}
