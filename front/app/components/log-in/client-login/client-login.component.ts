import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Router} from '@angular/router';
import { User } from 'src/app/common/entities/user';
import { LoginService } from 'src/app/services/login.service';
import { RegisterService } from 'src/app/services/register.service';

@Component({
  selector: 'app-client-login',
  templateUrl: './client-login.component.html',
  styleUrls: ['./client-login.component.css']
})
export class ClientLoginComponent implements OnInit{

  constructor(private router: Router, public loginService: LoginService, public registerService: RegisterService) { }

  titleMessage: string = '';
  signupMessage : string = '';

  ngOnInit(): void {
  }

  onSubmit(loginForm: NgForm){
    this.loginService.onLogin(loginForm.value.username, loginForm.value.password, this.router);
    loginForm.reset();
  }
  
  onSubmit2(registerForm: NgForm ) {

    let newClient = new User();
    newClient.username = registerForm.value['username'];
    newClient.password = registerForm.value['password'];
    newClient.emailAddress = registerForm.value['emailAddress'];
    newClient.userFirstName = registerForm.value['userFirstName'];
    newClient.userLastName = registerForm.value['userLastName'];
    newClient.typeOfUser = "client";
    console.log(newClient);

    // post client to backend to save in data base
    this.registerService.createUser(newClient).subscribe(
      event => {  // handle success
        this.titleMessage = 'Success';
        this.signupMessage = 'Successful registration. You can login now';
        console.log(newClient.userId);
        this.router.navigate(['/client']);
    });
}
}
