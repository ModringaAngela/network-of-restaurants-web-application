import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-administrator-login',
  templateUrl: './administrator-login.component.html',
  styleUrls: ['./administrator-login.component.css']
})
export class AdministratorLoginComponent implements OnInit {

  constructor(private router: Router, public loginService: LoginService) { }

  ngOnInit(): void {
  }

  onSubmit(loginForm: NgForm){
    this.loginService.onLogin(loginForm.value.username, loginForm.value.password, this.router);
    loginForm.reset();
  }

}
