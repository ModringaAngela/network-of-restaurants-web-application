import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Restaurant } from '../../common/entities/restaurant';
import { RestaurantService } from '../../services/restaurant.service';

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.css']
})
export class RestaurantComponent implements OnInit {

  restaurant: Restaurant = new Restaurant();

  constructor(private restaurantService: RestaurantService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(() => {
      this.handleRestaurantChoosen();
    })
  }

  handleRestaurantChoosen() {

    // get the "id" param string. convert string to a number using the "+" symbol
    const theRestaurantId: number = +this.route.snapshot.paramMap.get('id');

    this.restaurantService.getRestaurant(theRestaurantId).subscribe(
      data => {
        this.restaurant = data;
      }
    )
  }
}
