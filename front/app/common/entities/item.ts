export class Item{
    id:number;
    restaurantId: number;
    name:string;
    category:string;
    price:number;
    weight:number;
    observations:string;
    photo:string;
}
