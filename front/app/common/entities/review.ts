export class Review{
    restaurant_id: number;
    client_id: number;
    rating: number;
    comments:number;
    status:number;
}