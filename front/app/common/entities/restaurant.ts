export class Restaurant {
    id:number;
    restaurantName: string;
    restaurantAddress: string;
    restaurantProgram: string;
    numberOfTables: number;
    reviewRate: number;
    restaurantPhoto: string;
}
