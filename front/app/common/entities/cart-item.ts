import { Item } from './item';

export class CartItem {
    id: number;
    name: string;
    imageUrl: string;
    unitPrice: number;
    quantity: number;

    constructor(item: Item) {
        this.id = item.id;
        this.name = item.name;
        this.imageUrl = item.photo;
        this.unitPrice = item.price;

        this.quantity = 1;
    }
}
