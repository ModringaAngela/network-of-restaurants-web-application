export class User {
    userId: number;
    userFirstName: string;
    userLastName: string;
    userDob: string;
    typeOfUser: string;
    emailAddress: string;
    password:string;
    username:string;
}
