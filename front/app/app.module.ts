import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import{HttpClientModule} from '@angular/common/http';
import{AppRoutingModule, routingComponents} from './app-routing.module';
import {FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';;
import { RestaurantService } from './services/restaurant.service';
import { CartStatusComponent } from './components/cart-status/cart-status.component';
import { ReviewComponent } from './components/review/review.component';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    CartStatusComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule, 
    AppRoutingModule,
    FormsModule
  ],
  providers: [RestaurantService],
  bootstrap: [AppComponent]
})
export class AppModule { }
