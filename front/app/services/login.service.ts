import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import { User } from 'src/app/common/entities/user'; 
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  user: User;
  userType: string;

  constructor(private userService: UserService, private httpClient: HttpClient) {
  }

  onLogin(username: string, password: string, router: Router): boolean {

    let output: boolean = false;

    this.userService.getUserList().subscribe(
      data => {
        data.forEach( (user) => {
          if (user.username === username && user.password === password && user.typeOfUser === "client"){
            this.user = user;
            output = true;
            console.log(`${user.userId}`);
            router.navigate(['/client']);
          }
          
          if (user.username === username && user.password === password && user.typeOfUser === "administrator"){
            this.user = user;
            output = true;
            router.navigate(['/administrator']);
          }

          if (user.username === username && user.password === password && user.typeOfUser === "shipper"){
            this.user = user;
            output = true;
            router.navigate(['/shipper']);
          }
        });
      }
    );
    return output;
  }

  logout(): void {
    sessionStorage.removeItem('user');
    sessionStorage.removeItem('typeOfUser');
  }



}
