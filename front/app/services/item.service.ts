import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Item } from '../common/entities/item';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  private baseUrl = 'http://localhost:8080/api/items';

  constructor(private httpClient: HttpClient) { }

  getItemList(theRestaurantId:number): Observable<Item[]>{

    const restaurantUrl = `${this.baseUrl}/?id=${theRestaurantId}`;
    return this.httpClient.get<GetResponse>(restaurantUrl).pipe(
      map(response => response._embedded.items)
    )
  }
}

interface GetResponse{
  _embedded:{
    items: Item[];
  }
}