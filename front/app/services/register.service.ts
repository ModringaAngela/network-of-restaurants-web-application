import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from '../common/entities/user';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  baseUrl = 'http://localhost:8080/api/';
  
  constructor(private userService: UserService, private httpClient: HttpClient) { }

  // post request to save a client in the backend
  createUser(shipper: User): Observable<any> {
    return this.httpClient.post(this.baseUrl + 'users' + '/save', {
      username: shipper.username,
      password: shipper.password,
      emailAddress: shipper.emailAddress,
      userFirstName: shipper.userFirstName,
      userLastName: shipper.userLastName,
      userDob : shipper.userDob,
      typeOfUser: shipper.typeOfUser
    });
  }
}
