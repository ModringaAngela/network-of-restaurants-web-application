import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Restaurant } from '../common/entities/restaurant';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {

  private baseUrl = 'http://localhost:8080/api/restaurants';

  constructor(private httpClient: HttpClient) { }

  getRestaurant(theRestaurantId: number):Observable<Restaurant>{
    const productUrl = `${this.baseUrl}/${theRestaurantId}`;
    return this.httpClient.get<Restaurant>(productUrl);
  }

  getRestaurantList(): Observable<Restaurant[]>{
    return this.httpClient.get<GetResponse>(this.baseUrl).pipe(
      map(response => response._embedded.restaurants)
    )
  }
}

interface GetResponse{
  _embedded:{
    restaurants: Restaurant[];
  }
}